module org.algo_impl {
    requires javafx.controls;
    requires javafx.fxml;

    opens org.algo_impl to javafx.fxml;
    exports org.algo_impl;
}